<?php
function retornaSQLInsertDeArray($nomeTabela, $colunas, $removerColunas = TRUE) {
	$rsColunas = $colunas;
	$strInsert = "insert into " . $nomeTabela . " ( " . implode ( ", ", array_keys ( $rsColunas ) ) . " ) values ( :" . implode ( ", :", array_keys ( $rsColunas ) ) . " )";
	return $strInsert;
}
function retornaSQLUpdateDeArray($nomeTabela, $colunas, $removerColunas = TRUE) {
	$rsColunas = $colunas;
	$strUpdate = '';
	foreach ( array_keys ( $rsColunas ) as $coluna ) {
		$strUpdate .= ' ' . $coluna . ' = :' . $coluna . ',';
	}
	$size = strlen ( $strUpdate );
	$strUpdate = substr ( $strUpdate, 0, $size - 1 ); // Removendo a ultima virgula
	$strUpdate = "update " . $nomeTabela . " set " . $strUpdate . " WHERE id = :id ";
	return $strUpdate;
}
function errorHandler($e) {
	$ErrMsg = $e->getMessage ();
	$data = array (
			'Code' => '1',
			'Message' => $ErrMsg 
	);
	return $data;
}

$app->options ( '/{routes:.+}', function ($request, $response, $args) {
	return $response;
} );

$app->add ( function ($req, $res, $next) {
	$response = $next ( $req, $res );
	return $response->withHeader ( 'Access-Control-Allow-Origin', '*' )->withHeader ( 'Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization' )->withHeader ( 'Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS' );
} );

/**
 * *********************************************************** user ************************************************************
 */
$app->get ( '/user/byemail/[{query}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$sth = $this->db->prepare ( "SELECT * FROM user WHERE UPPER(email) = UPPER(:query)" );
		$query = $args ['query'];
		$sth->bindParam ( "query", $query );
		$sth->execute ();
		$res = $sth->fetchAll ();
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->post ( '/user', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$input = $request->getParsedBody ();
		
		// Inclusão
		$sql = retornaSQLInsertDeArray ( 'user', $input );
		// return 'SQL:' . $sql;
		$sth = $this->db->prepare ( $sql );
		foreach ( array_keys ( $input ) as $key ) {
			$sth->bindParam ( $key, $input [$key] );
		}
		$sth->execute ();
		$id = $this->db->lastInsertId ();
		
		$sth = $container->db->prepare ( "SELECT * FROM user WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->put ( '/user/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		$input = $request->getParsedBody ();
		
		$sql = retornaSQLupdateDeArray ( 'user', $input );
		$sth = $this->db->prepare ( $sql );
		
		$sth->bindParam ( 'id', $id );
		foreach ( array_keys ( $input ) as $key ) {
			$sth->bindParam ( $key, $input [$key] );
		}
		
		$sth->execute ();
		
		$sth = $container->db->prepare ( "SELECT * FROM user WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->delete ( '/user/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "delete FROM user WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = [ ];
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

/**
 * *********************************************************** COMPANY ************************************************************
 */

$app->get ( '/company', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		$sth = $container->db->prepare ( "SELECT * FROM company WHERE 1=1" );
		$sth->execute ();
		$res = $sth->fetchAll ();
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->post ( '/company', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$input = $request->getParsedBody ();
		
		// Inclusão
		$sql = retornaSQLInsertDeArray ( 'company', $input );
		// return 'SQL:' . $sql;
		$sth = $this->db->prepare ( $sql );
		foreach ( array_keys ( $input ) as $key ) {
			$sth->bindParam ( $key, $input [$key] );
		}
		$sth->execute ();
		$id = $this->db->lastInsertId ();
		
		$sth = $container->db->prepare ( "SELECT * FROM company WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->put ( '/company/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		$input = $request->getParsedBody ();
		
		$sql = retornaSQLupdateDeArray ( 'company', $input );
		$sth = $this->db->prepare ( $sql );
		
		$sth->bindParam ( 'id', $id );
		foreach ( array_keys ( $input ) as $key ) {
			$sth->bindParam ( $key, $input [$key] );
		}
		
		$sth->execute ();
		
		$sth = $container->db->prepare ( "SELECT * FROM company WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->delete ( '/company/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "delete FROM company WHERE id = :id " );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = [ ];
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/full-company', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		$sth = $container->db->prepare ( "SELECT * FROM company WHERE 1 = 1" );
		$sth->execute ();
		$res = $sth->fetchAll ();
		/*
		 * foreach ( $res as $key => $value ) {
		 * $sthComposition = $container->db->prepare ( "SELECT * FROM item_composition WHERE item_id = :item_id" );
		 * $sthComposition->bindParam ( "item_id", $res [$key] ['id'] );
		 * $sthComposition->execute ();
		 * $resComposition = $sthComposition->fetchAll ();
		 * $res [$key] ['composition'] = $resComposition;
		 * $sthSubstitute = $container->db->prepare ( "SELECT * FROM item_substitute WHERE item_id = :item_id" );
		 * $sthSubstitute->bindParam ( "item_id", $res [$key] ['id'] );
		 * $sthSubstitute->execute ();
		 * $resSubstitute = $sthSubstitute->fetchAll ();
		 * $res [$key] ['substitute'] = $resSubstitute;
		 * }
		 */
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

/**
 * *********************************************************** user_company ************************************************************
 */

$app->get ( '/company-user[/{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		if ($id == null) {
			$sth = $container->db->prepare ( " SELECT * FROM company_user WHERE 1 = 1 " );
		} else {
			$sth = $container->db->prepare ( " SELECT * FROM company_user WHERE 1 = 1 and id = :id " );
			$sth->bindParam ( "id", $id );
		}
		
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/company-user/non-client/[{companyid}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$companyid = $request->getAttribute ( 'companyid' );
		$perfil = $request->getAttribute ( 'perfil' );
		
		$sth = $container->db->prepare ( " SELECT company_user.id id
											    , company_user.company_id company_id
											    , company_user.perfil perfil
											    , company_user.tf_checked_by tf_checked_by
											    , company_user.checked_by_user_id checked_by_user_id
											    , company_user.user_id user_id
											    , user.username username
											    , user.name name
											    , user.email email
											    , user.img_url img_url
											 FROM company_user
											 LEFT JOIN user ON company_user.user_id = user.id
											WHERE company_user.company_id = :pCompanyID and company_user.perfil != 'C' " );
		$sth->bindParam ( "pCompanyID", $companyid );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/company-user/bycompany/[{companyid}[/{perfil}]]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$companyid = $request->getAttribute ( 'companyid' );
		$perfil = $request->getAttribute ( 'perfil' );
		
		$sth = $container->db->prepare ( " SELECT * FROM company_user WHERE company_id = :pCompanyID and perfil like :pPerfil " );
		$perfil = "%" . $perfil . "%";
		$sth->bindParam ( "pCompanyID", $companyid );
		$sth->bindParam ( "pPerfil", $perfil );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->post ( '/company-user', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$input = $request->getParsedBody ();
		
		// Inclusão
		$sql = retornaSQLInsertDeArray ( 'company_user', $input );
		// return 'SQL:' . $sql;
		$sth = $this->db->prepare ( $sql );
		foreach ( array_keys ( $input ) as $key ) {
			$sth->bindParam ( $key, $input [$key] );
		}
		$sth->execute ();
		$id = $this->db->lastInsertId ();
		
		$sth = $container->db->prepare ( "SELECT * FROM company_user WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->put ( '/company-user/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		$input = $request->getParsedBody ();
		
		$sql = retornaSQLupdateDeArray ( 'company_user', $input );
		$sth = $this->db->prepare ( $sql );
		
		$sth->bindParam ( 'id', $id );
		foreach ( array_keys ( $input ) as $key ) {
			$sth->bindParam ( $key, $input [$key] );
		}
		
		$sth->execute ();
		
		$sth = $container->db->prepare ( "SELECT * FROM company_user WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->delete ( '/company-user/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "delete FROM company_user WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = [ ];
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

/**
 * *********************************************************** ITENS ************************************************************
 */

$app->get ( '/item', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		$sth = $container->db->prepare ( "SELECT * FROM item WHERE 1=1" );
		$sth->execute ();
		$res = $sth->fetchAll ();
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/item/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "SELECT * FROM item WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/item/bydescription/[{query}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$sth = $this->db->prepare ( "SELECT * FROM item WHERE UPPER(description) LIKE UPPER(:query) ORDER BY description" );
		$query = "%" . $args ['query'] . "%";
		$sth->bindParam ( "query", $query );
		$sth->execute ();
		$res = $sth->fetchAll ();
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->post ( '/item', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$input = $request->getParsedBody ();
		
		// Inclusão
		$sql = retornaSQLInsertDeArray ( 'item', $input );
		// return 'SQL:' . $sql;
		$sth = $this->db->prepare ( $sql );
		foreach ( array_keys ( $input ) as $key ) {
			$sth->bindParam ( $key, $input [$key] );
		}
		$sth->execute ();
		$id = $this->db->lastInsertId ();
		
		$sth = $container->db->prepare ( "SELECT * FROM item WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->put ( '/item/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		$input = $request->getParsedBody ();
		
		$sql = retornaSQLupdateDeArray ( 'item', $input );
		$sth = $this->db->prepare ( $sql );
		
		$sth->bindParam ( 'id', $id );
		foreach ( array_keys ( $input ) as $key ) {
			$sth->bindParam ( $key, $input [$key] );
		}
		
		$sth->execute ();
		
		$sth = $container->db->prepare ( "SELECT * FROM item WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->delete ( '/item/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "delete FROM item WHERE id = :id " );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = [ ];
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/item/bycompany/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "SELECT * FROM item WHERE company_id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/full-item', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		$sth = $container->db->prepare ( "SELECT * FROM item WHERE 1 = 1" );
		$sth->execute ();
		$res = $sth->fetchAll ();
		foreach ( $res as $key => $value ) {
			$sthComposition = $container->db->prepare ( "SELECT * FROM item_composition WHERE item_id = :item_id" );
			$sthComposition->bindParam ( "item_id", $res [$key] ['id'] );
			$sthComposition->execute ();
			$resComposition = $sthComposition->fetchAll ();
			$res [$key] ['composition'] = $resComposition;
			$sthSubstitute = $container->db->prepare ( "SELECT * FROM item_substitute WHERE item_id = :item_id" );
			$sthSubstitute->bindParam ( "item_id", $res [$key] ['id'] );
			$sthSubstitute->execute ();
			$resSubstitute = $sthSubstitute->fetchAll ();
			$res [$key] ['substitute'] = $resSubstitute;
		}
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/full-item/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		$id = $request->getAttribute ( 'id' );
		$sth = $container->db->prepare ( "SELECT * FROM item WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		foreach ( $res as $key => $value ) {
			$sthComposition = $container->db->prepare ( "SELECT * FROM item_composition WHERE item_id = :item_id" );
			$sthComposition->bindParam ( "item_id", $res [$key] ['id'] );
			$sthComposition->execute ();
			$resComposition = $sthComposition->fetchAll ();
			$res [$key] ['composition'] = $resComposition;
			$sthSubstitute = $container->db->prepare ( "SELECT * FROM item_substitute WHERE item_id = :item_id" );
			$sthSubstitute->bindParam ( "item_id", $res [$key] ['id'] );
			$sthSubstitute->execute ();
			$resSubstitute = $sthSubstitute->fetchAll ();
			$res [$key] ['substitute'] = $resSubstitute;
		}
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/full-item/bycompany/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		$id = $request->getAttribute ( 'id' );
		$sth = $container->db->prepare ( "SELECT * FROM item WHERE company_id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		foreach ( $res as $key => $value ) {
			$sthComposition = $container->db->prepare ( "SELECT * FROM item_composition WHERE item_id = :item_id" );
			$sthComposition->bindParam ( "item_id", $res [$key] ['id'] );
			$sthComposition->execute ();
			$resComposition = $sthComposition->fetchAll ();
			$res [$key] ['composition'] = $resComposition;
			$sthSubstitute = $container->db->prepare ( "SELECT * FROM item_substitute WHERE item_id = :item_id" );
			$sthSubstitute->bindParam ( "item_id", $res [$key] ['id'] );
			$sthSubstitute->execute ();
			$resSubstitute = $sthSubstitute->fetchAll ();
			$res [$key] ['substitute'] = $resSubstitute;
		}
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

/**
 * *********************************************************** item-composition ************************************************************
 */

$app->get ( '/item-composition', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		$sth = $container->db->prepare ( "SELECT * FROM item_composition WHERE 1=1" );
		$sth->execute ();
		$res = $sth->fetchAll ();
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/item-composition/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "SELECT * FROM item_composition WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->post ( '/item-composition', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$input = $request->getParsedBody ();
		
		// Inclusão
		$sql = retornaSQLInsertDeArray ( 'item_composition', $input );
		// return 'SQL:' . $sql;
		$sth = $this->db->prepare ( $sql );
		foreach ( array_keys ( $input ) as $key ) {
			$sth->bindParam ( $key, $input [$key] );
		}
		$sth->execute ();
		$id = $this->db->lastInsertId ();
		
		$sth = $container->db->prepare ( "SELECT * FROM item_composition WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->put ( '/item-composition/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		$input = $request->getParsedBody ();
		
		$sql = retornaSQLupdateDeArray ( 'item_composition', $input );
		$sth = $this->db->prepare ( $sql );
		
		$sth->bindParam ( 'id', $id );
		foreach ( array_keys ( $input ) as $key ) {
			$sth->bindParam ( $key, $input [$key] );
		}
		
		$sth->execute ();
		
		$sth = $container->db->prepare ( "SELECT * FROM item_composition WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->delete ( '/item-composition/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "delete FROM item_composition WHERE id = :id " );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = [ ];
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/item-composition/byitem/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "SELECT * FROM item_composition WHERE item_id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

/**
 * *********************************************************** item-substitute ************************************************************
 */

$app->get ( '/item-substitute', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		$sth = $container->db->prepare ( "SELECT * FROM item_substitute WHERE 1=1" );
		$sth->execute ();
		$res = $sth->fetchAll ();
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/item-substitute/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "SELECT * FROM item_substitute WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->post ( '/item-substitute', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$input = $request->getParsedBody ();
		
		// Inclusão
		$sql = retornaSQLInsertDeArray ( 'item_substitute', $input );
		// return 'SQL:' . $sql;
		$sth = $this->db->prepare ( $sql );
		foreach ( array_keys ( $input ) as $key ) {
			$sth->bindParam ( $key, $input [$key] );
		}
		$sth->execute ();
		$id = $this->db->lastInsertId ();
		
		$sth = $container->db->prepare ( "SELECT * FROM item_substitute WHERE id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->delete ( '/item-substitute/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "delete FROM item_substitute WHERE id = :id " );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = [ ];
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );

$app->get ( '/item-substitute/byitem/[{id}]', function ($request, $response, $args) use ($app) {
	try {
		$header = $request->getHeaderLine ( 'AUTHORIZATION' );
		
		$auth = new auth ();
		$auth->setApp ( $app );
		$container = $app->getContainer ();
		if ($auth->verify ( $header ) == false) {
			$container->logger->error ( "'Code' => '401' 'Message' => 'UNAUTHORIZED'" . $header );
			$data = array (
					'Code' => '401',
					'Message' => 'UNAUTHORIZED' 
			);
			return $response->withStatus ( 401 )->withJson ( $data );
		}
		
		$id = $request->getAttribute ( 'id' );
		
		$sth = $container->db->prepare ( "SELECT * FROM item_substitute WHERE item_id = :id" );
		$sth->bindParam ( "id", $id );
		$sth->execute ();
		$res = $sth->fetchAll ();
		
		return $this->response->withJson ( $res );
	} catch ( Exception $e ) {
		return $response->withStatus ( 500 )->withJson ( errorHandler ( $e ) );
	}
} );
	