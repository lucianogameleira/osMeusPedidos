import { FormControl } from '@angular/forms';

export class UserValidator {

	wsreturn : any = [];

	constructor( ) {
	}

   static isValidMail(control: FormControl){
        let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

        if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return { "Please provide a valid email": true };
        }

        return null;
    }

    static isValidAge(control: FormControl): any {
 
        if(isNaN(control.value)){
            return {
                "not a number": true
            };
        }
 
        if(control.value % 1 !== 0){
            return {
                "not a whole number": true
            };
        }
 
        if(control.value < 18){
            return {
                "too young": true
            };
        }
 
        if (control.value > 120){
            return {
                "not realistic": true
            };
        }
 
        return null;
    }
 
}