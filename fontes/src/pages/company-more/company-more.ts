import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { GlobalProvider }  from '../../providers/global-provider';
import { CompanyProvider }  from '../../providers/company-provider';

import { CompanyCashierPage } from '../company-cashier/company-cashier';
import { CompanyCookPage } from '../company-cook/company-cook';
import { CompanyAreaPage } from '../company-area/company-area';
import { CompanyBartenderPage } from '../company-bartender/company-bartender';
import { CompanyBedroomPage } from '../company-bedroom/company-bedroom';
import { CompanyTablePage } from '../company-table/company-table';

import { CompanyUserProvider }  from '../../providers/company-user-provider';

@Component({
  selector: 'page-company-more',
  templateUrl: 'company-more.html'
})

export class CompanyMorePage {

  constructor( public navCtrl         : NavController
             , public navParams       : NavParams
             , public global          : GlobalProvider
             , public companyService  : CompanyProvider
             , public companyUserService   : CompanyUserProvider ) {
  }

  ionViewWillEnter() { 
      this.companyUserService.getByCompanyUser();
      console.log('ionViewDidLoad CompanyMorePage'); 
  }

  companyCashierPage(){
    this.navCtrl.push(CompanyCashierPage);    
  }

  companyCookPage(){
    this.navCtrl.push(CompanyCookPage);    
  }

  companyAreaPage(){
    this.navCtrl.push(CompanyAreaPage);    
  }

  companyBartenderPage(){
    this.navCtrl.push(CompanyBartenderPage);    
  }

  companyBedroomPage(){
    this.navCtrl.push(CompanyBedroomPage);    
  }
  
  companyTablePage(){
    this.navCtrl.push(CompanyTablePage);    
  }

}


// WEBPACK FOOTER //
// ./src/pages/company-more/company-more.ts


// WEBPACK FOOTER //
// ./src/pages/company-more/company-more.ts


// WEBPACK FOOTER //
// ./src/pages/company-more/company-more.ts