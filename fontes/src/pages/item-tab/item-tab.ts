import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ItemListPage } from '../item-list/item-list';
import { ItemMainPage } from '../item-main/item-main';
import { ItemCompositionPage } from '../item-composition/item-composition';
import { ItemSubstitutePage } from '../item-substitute/item-substitute';


@Component({
  selector: 'page-item-tab',
  templateUrl: 'item-tab.html'
})
export class ItemTabPage {

  itemTab1Root: any = ItemListPage;
  itemTab2Root: any = ItemMainPage;
  itemTab3Root: any = ItemCompositionPage;
  itemTab4Root: any = ItemSubstitutePage;

  mySelectedItemTabIndex: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.mySelectedItemTabIndex = navParams.data.tabIndex || 0;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ItemTabPage');
  }

}
