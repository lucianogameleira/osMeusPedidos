import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { GlobalProvider }  from '../../providers/global-provider';
import { ItemProvider }  from '../../providers/item-provider';
import { ItemCompositionProvider }  from '../../providers/item-composition-provider';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import { Http } from '@angular/http';


@Component({
  selector: 'page-item-composition',
  templateUrl: 'item-composition.html'
})

export class ItemCompositionPage {

  public compositionSelectedList : any = [];
  public compositionToSelectList : any = [];
  public compositionToSelectFullList : any =[];

  constructor( public http: Http
             , public navCtrl     : NavController
             , public navParams   : NavParams
             , public global      : GlobalProvider
             , public itemService : ItemProvider
             , public itemCompositionService : ItemCompositionProvider ) {

  }

  private loadCompositionSelectedList ( list : any ) {
    
    this.compositionSelectedList      = [];
    this.compositionToSelectFullList  = this.itemService.fullList.slice(0);

    this.compositionToSelectFullList = this.compositionToSelectFullList.filter(( obj : any ) => {
        return ( obj.id != this.itemService.selected.id );
    });

    this.compositionToSelectList      = this.compositionToSelectFullList.slice(0);

    let objIndex  : number;
    for ( let index : number = 0; index < list.length; index++) {
        objIndex = this.compositionToSelectFullList.findIndex(( (obj : any ) => ( obj.id == list[index].sub_item_id && obj.id != this.itemService.selected.id ) ));
        this.global.addToList( this.compositionSelectedList, this.compositionToSelectFullList [ objIndex ] );
        this.global.deleteFromList( this.compositionToSelectList, this.compositionToSelectFullList [ objIndex ] );
    }
  }

  /* ionViewDidLoad... https://ionicframework.com/docs/v2/api/navigation/NavController/ */
  
  ionViewWillEnter() {
    if ( this.itemService.selected.id !== null) {
      this.itemCompositionService.getItensFromItemSelected();
      this.loadCompositionSelectedList( this.itemCompositionService.fullList );
    } else {
      this.compositionSelectedList = [];
    }
  }

  public add( item : any ) {

    let itemComposition : any = {};
    itemComposition.item_id = this.itemService.selected.id;
    itemComposition.sub_item_id = item.id;

    this.itemCompositionService.post( itemComposition )
                               .subscribe ( ( data : any ) => { let objIndex : number = this.compositionToSelectFullList.findIndex(( (obj : any ) => obj.id == data.sub_item_id ));
                                                                this.global.addToList( this.compositionSelectedList, this.compositionToSelectFullList[objIndex] );
                                                                this.global.deleteFromList( this.compositionToSelectList, this.compositionToSelectFullList[objIndex] );
                                                              }
                                          , ( err  : any ) => { this.global.handleError("ERROR_SAVING", err); } );

  }

  public delete( item : any ) {
    
    let objIndex : number = this.itemCompositionService.fullList.findIndex(( (obj : any ) => ( obj.item_id == this.itemService.selected.id && obj.sub_item_id == item.id ) )); 
    let itemComposition : any = this.itemCompositionService.fullList[ objIndex ];

    this.itemCompositionService.delete( itemComposition )
                               .subscribe ( ( data : any ) => { this.global.addToList( this.compositionToSelectList, item );
                                                                this.global.deleteFromList( this.compositionSelectedList, item );
                                                              }
                                          , ( err  : any ) => { this.global.handleError("ERROR_SAVING", err); } );
  }

  public filter( ev : any ) {

    this.compositionToSelectList = this.compositionToSelectFullList.slice(0);
    var val = ev.target.value;
    if (val && val.trim() != '') {
      this.compositionToSelectList = this.compositionToSelectList.filter(( obj : any ) => {
        return (obj.description.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }

  }

}