import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ClientMainPage } from '../client-main/client-main';

@Component({
  selector: 'page-client-tab',
  templateUrl: 'client-tab.html'
})
export class ClientTabPage {

  clientTab1Root: any = ClientMainPage;

  mySelectedItemTabIndex: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.mySelectedItemTabIndex = navParams.data.tabIndex || 0;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ClientTabPage');
  }

}



// WEBPACK FOOTER //
// ./src/pages/client-tab/client-tab.ts