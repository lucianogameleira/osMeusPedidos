import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { CompanyListPage } from '../company-list/company-list';
import { CompanyMainPage } from '../company-main/company-main';
import { CompanyMorePage } from '../company-more/company-more';

@Component({
  selector: 'page-company-tab',
  templateUrl: 'company-tab.html'
})
export class CompanyTabPage {

  companyTab1Root: any = CompanyListPage;
  companyTab2Root: any = CompanyMainPage;
  companyTab3Root: any = CompanyMorePage;

  mySelectedCompanyTabIndex: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.mySelectedCompanyTabIndex = navParams.data.tabIndex || 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyTabPage');
  }

}