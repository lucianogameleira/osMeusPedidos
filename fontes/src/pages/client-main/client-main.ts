import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-client-main',
  templateUrl: 'client-main.html'
})
export class ClientMainPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ClientMainPage');
  }

}



// WEBPACK FOOTER //
// ./src/pages/client-main/client-main.ts