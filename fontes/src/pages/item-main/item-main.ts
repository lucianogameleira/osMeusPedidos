import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import {Validators, FormBuilder } from '@angular/forms';

import { GlobalProvider }  from '../../providers/global-provider';
import { ItemProvider }  from '../../providers/item-provider';

@Component({
  selector: 'page-item-main',
  templateUrl: 'item-main.html'
})

export class ItemMainPage {

  itemPrincipalForm : any = {};
  img_urlSelected : string;

  public unitList : any = [];

  constructor( public navCtrl: NavController
             , public navParams: NavParams
             , public global: GlobalProvider
             , public formBuilder : FormBuilder
             , public itemService : ItemProvider
             ) {
               
    this.itemPrincipalForm = this.formBuilder.group( { id : []
                                                     , company_id : []
                                                     , description : [null, Validators.compose( [ Validators.minLength(4)
                                                                                                , Validators.required
                                                                                               ] 
                                                                                             )
                                                                    ]
                                                     , type : [null, Validators.compose( [ Validators.required ] ) ]
                                                     , unity : [null, Validators.compose( [ Validators.required ] ) ]
                                                     , quantity : ['1', Validators.compose( [ Validators.required ] ) ]
                                                     , tf_need_preparation : ['true', Validators.compose( [ Validators.required ] ) ]
                                                     , tf_allows_cooking_steak : ['false', Validators.compose( [ Validators.required ] ) ]
                                                     , preparation_time_min : ['1', Validators.compose( [ Validators.required ] ) ]
                                                     , img_url : []
                                                    });
                                                               
  }

  loadingSelected( item : any) {
    if ( item != 'undefined') {
      this.img_urlSelected =  item.img_url; 
      this.itemPrincipalForm.get( 'img_url' ).setValue( item.img_url );
      this.itemPrincipalForm.get( 'id' ).setValue( item.id );
      this.itemPrincipalForm.get( 'description' ).setValue( item.description );
      this.itemPrincipalForm.get( 'type' ).setValue( item.type );
      this.itemPrincipalForm.get( 'unity' ).setValue( item.unity );
      this.itemPrincipalForm.get( 'quantity' ).setValue( item.quantity );
      this.itemPrincipalForm.get( 'tf_need_preparation' ).setValue( item.tf_need_preparation );
      this.itemPrincipalForm.get( 'tf_allows_cooking_steak' ).setValue( item.tf_allows_cooking_steak );
      this.itemPrincipalForm.get( 'preparation_time_min' ).setValue( item.preparation_time_min );
      this.itemPrincipalForm.get( 'company_id' ).setValue( item.company_id );
    }
  }

  ionViewWillEnter () {
    this.loadingSelected( this.itemService.selected );

  }

  ionViewDidLoad() {
    this.unitList = this.itemService.liquidUnitList;
    this.unitList = this.unitList.concat( this.itemService.solidUnitList );
  }

  typeChange( typeItem : string ) { 

    var objIndex : number = this.itemService.typeList.findIndex(( (obj : any ) => obj.value == typeItem ));

    if ( this.itemService.typeList[objIndex].solid ) {
      this.unitList = this.itemService.solidUnitList;
    } else {
      this.unitList = this.itemService.liquidUnitList;
    }
  } 
  
  postForm (){

    this.itemPrincipalForm.value.tf_need_preparation = this.itemPrincipalForm.value.tf_need_preparation.toString();
    this.itemPrincipalForm.value.tf_allows_cooking_steak = this.itemPrincipalForm.value.tf_allows_cooking_steak.toString();
    // Solid or Liquid ?
    let objIndex : number = this.itemService.typeList.findIndex( ( (obj : any ) => ( obj.value == this.itemPrincipalForm.value.type ) ) ); 
    this.itemPrincipalForm.value.tf_solid = this.itemService.typeList[objIndex].solid.toString() ;

    this.itemService.save( this.itemPrincipalForm.value )
                    .subscribe ( ( data : any ) => { this.loadingSelected( data );
                                                     this.itemPrincipalForm.reset(); 
                                                   }
                               , ( err  : any ) => { this.global.handleError("ERROR_SAVING", err); } );

  }

  getPicture(pPictureSourceType : number) {
      this.global.getPicture( pPictureSourceType )
                 .then( image => {     
                                   let newFileName : string = this.global.createFileName();
                                   this.global.uploadImage( image, newFileName, 'item' )
                                              .then ( ( data : any ) => {  this.img_urlSelected = this.global.serverAdress + '/image/item/'+ newFileName;
                                                                          this.itemPrincipalForm.get( 'img_url' ).setValue( this.img_urlSelected ); 
                                                                        }
                                                   , ( err : any ) => { this.global.handleError( "ERROR_UPLOADING_FILE", err ); } );
                                  }
                      , error => {
                                    this.global.msg( "ERROR_SELECTING_PICTURE" );
                                 });
  }

}