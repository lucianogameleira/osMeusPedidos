import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import {Validators, FormBuilder, FormControl } from '@angular/forms';

import { GlobalProvider }  from '../../providers/global-provider';
import { CompanyProvider }  from '../../providers/company-provider';

@Component({
  selector: 'page-company-main',
  templateUrl: 'company-main.html'
})

export class CompanyMainPage {

  companyPrincipalForm : any = {};
  img_urlSelected : string;
  
  public mask = [ /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/' ,/\d/ ,/\d/ ,/\d/, /\d/, '-' ,/\d/, /\d/];

  public unitList : any = [];

  constructor( public navCtrl: NavController
             , public navParams: NavParams
             , public global: GlobalProvider
             , public formBuilder : FormBuilder
             , public companyService : CompanyProvider
             ) {
               
    this.companyPrincipalForm = this.formBuilder.group( { id : []
                                                     , description : [null, Validators.compose( [ Validators.minLength(4)
                                                                                                , Validators.required
                                                                                               ] 
                                                                                             )
                                                                    ]
                                                     , address : []
                                                     , cnpj : [null, Validators.compose( [ this.validarCNPJ ] 
                                                                                        )
                                                              ]
                                                     , estate_inscription : []
                                                     , img_url : []
                                                     , max_discount_percentage : []
                                                     , type : []
                                                     , accountable_user_id : []
                                                    });
                                                               
  }

  loadingSelected( company : any) {
    if ( company != 'undefined') {
      this.img_urlSelected =  company.img_url; 
      this.companyPrincipalForm.get( 'img_url' ).setValue( company.img_url );
      this.companyPrincipalForm.get( 'id' ).setValue( company.id );
      this.companyPrincipalForm.get( 'description' ).setValue( company.description );
      this.companyPrincipalForm.get( 'type' ).setValue( company.type );
      this.companyPrincipalForm.get( 'address' ).setValue( company.address );
      this.companyPrincipalForm.get( 'cnpj' ).setValue( company.cnpj );
      this.companyPrincipalForm.get( 'estate_inscription' ).setValue( company.estate_inscription );
      this.companyPrincipalForm.get( 'max_discount_percentage' ).setValue( company.max_discount_percentage );
      this.companyPrincipalForm.get( 'accountable_user_id' ).setValue( company.accountable_user_id );
    }
  }

  ionViewWillEnter () {
    this.loadingSelected( this.companyService.selected );

  }

  ionViewDidLoad() {
    this.unitList = this.companyService.typeList.slice(0);
  }

  typeChange( typeCompany : string ) { 
/*
    var objIndex : number = this.companyService.typeList.findIndex(( (obj : any ) => obj.value == typeCompany ));

    if ( this.companyService.typeList[objIndex].solid ) {
      this.unitList = this.companyService.solidUnitList;
    } else {
      this.unitList = this.companyService.liquidUnitList;
    }
    */
  } 
  
  postForm (){

    //this.companyPrincipalForm.value.tf_need_preparation = this.companyPrincipalForm.value.tf_need_preparation.toString();
    //this.companyPrincipalForm.value.tf_allows_cooking_steak = this.companyPrincipalForm.value.tf_allows_cooking_steak.toString();

    this.companyService.save( this.companyPrincipalForm.value )
                    .subscribe ( ( data : any ) => { this.loadingSelected( data );
                                                     this.companyPrincipalForm.reset(); 
                                                   }
                               , ( err  : any ) => { this.global.handleError("ERROR_SAVING", err); } );

  }

  getPicture(pPictureSourceType : number) {
      this.global.getPicture( pPictureSourceType )
                 .then( image => {     
                                   let newFileName : string = this.global.createFileName();
                                   this.global.uploadImage( image, newFileName, 'company' )
                                              .then ( ( data : any ) => {  this.img_urlSelected = this.global.serverAdress + '/image/company/'+ newFileName;
                                                                          this.companyPrincipalForm.get( 'img_url' ).setValue( this.img_urlSelected ); 
                                                                        }
                                                   , ( err : any ) => { this.global.handleError( "ERROR_UPLOADING_FILE", err ); } );
                                  }
                      , error => {
                                    this.global.msg( "ERROR_SELECTING_PICTURE" );
                                 });
  }

  validarCNPJ( control: FormControl) : any {

      if ( control.value == null ) { return {"invalidCPF": true}; }
  
      let cnpj : string = control.value;

      cnpj = cnpj.replace(/[^\d]+/g,'');

      if(cnpj == '') return {"invalidCPF": true};

      if (cnpj.length != 14) { return {"invalidCPF": true}; }

      // Elimina CNPJs invalidos conhecidos
      if (cnpj == "00000000000000" || 
          cnpj == "11111111111111" || 
          cnpj == "22222222222222" || 
          cnpj == "33333333333333" || 
          cnpj == "44444444444444" || 
          cnpj == "55555555555555" || 
          cnpj == "66666666666666" || 
          cnpj == "77777777777777" || 
          cnpj == "88888888888888" || 
          cnpj == "99999999999999") {
              return {"invalidCPF": true};
          }          

      // Valida DVs
      let tamanho : number = cnpj.length - 2
      let numeros : string = cnpj.substring(0,tamanho);
      let digitos : string = cnpj.substring(tamanho);
      let soma    : number = 0;
      let pos     : number = tamanho - 7;
      let i       : number = 0;
      let resultado : number = 0;
      for (i = tamanho; i >= 1; i--) {
        soma += parseInt(numeros.charAt(tamanho - i)) * pos--;
        if (pos < 2)
              pos = 9;
      }
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != parseInt( digitos.charAt(0) )) {
          return {"invalidCPF": true};
      }

      tamanho = tamanho + 1;
      numeros = cnpj.substring(0,tamanho);
      soma = 0;
      pos = tamanho - 7;
      for (i = tamanho; i >= 1; i--) {
        soma += parseInt( numeros.charAt(tamanho - i) ) * pos--;
        if (pos < 2)
              pos = 9;
      }
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != parseInt( digitos.charAt(1))) {
        return {"invalidCPF": true};
      }

      return null;

  }

}