import { Component } from '@angular/core';
import { MenuController, NavController, Slides } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ClientTabPage } from '../client-tab/client-tab';
import { GlobalProvider }  from '../../providers/global-provider';

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})

export class TutorialPage {
  showSkip = true;

    constructor( public navCtrl: NavController
               , public menu: MenuController
               , public storage: Storage
               , public global: GlobalProvider
    ) { }

  startApp() {
    this.navCtrl.push(ClientTabPage).then(() => {
      this.storage.set('hasSeenTutorial', 'true');
    })
  }

  onSlideChangeStart(slider: Slides) {
    this.showSkip = !slider.isEnd();
  }

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  ionViewDidLeave() {
    this.menu.enable(true);
  }

}
