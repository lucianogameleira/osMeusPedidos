import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { GlobalProvider }  from '../../providers/global-provider';
import { ItemProvider }  from '../../providers/item-provider';
import { ItemSubstituteProvider }  from '../../providers/item-substitute-provider';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import { Http } from '@angular/http';


@Component({
  selector: 'page-item-substitute',
  templateUrl: 'item-substitute.html'
})

export class ItemSubstitutePage {

  public substituteSelectedList : any = [];
  public substituteToSelectList : any = [];
  public substituteToSelectFullList : any =[];

  constructor( public http: Http
             , public navCtrl     : NavController
             , public navParams   : NavParams
             , public global      : GlobalProvider
             , public itemService : ItemProvider
             , public itemSubstituteService : ItemSubstituteProvider ) {
  }

  private loadSubstituteSelectedList ( list : any ) {
    
    let objIndex  : number;

    this.substituteSelectedList      = [];
    this.substituteToSelectFullList  = this.itemService.fullList.slice(0);
    // Removing Actual Item
    objIndex = this.substituteToSelectFullList.findIndex(( (obj : any ) => obj.id == this.itemService.selected.id ));
    this.global.deleteFromList( this.substituteToSelectFullList, this.substituteToSelectFullList[objIndex] ); 
    // Filtering the same Type
    this.substituteToSelectFullList = this.substituteToSelectFullList.filter(( obj : any ) => {
        return ( obj.tf_solid == this.itemService.selected.tf_solid );
    });

    this.substituteToSelectList      = this.substituteToSelectFullList.slice(0);

    for ( let index : number = 0; index < list.length; index++) {
        objIndex = this.substituteToSelectFullList.findIndex(( (obj : any ) => obj.id == list[index].substitute_item_id ));
        this.global.addToList( this.substituteSelectedList, this.substituteToSelectFullList [ objIndex ] );
        this.global.deleteFromList( this.substituteToSelectList, this.substituteToSelectFullList [ objIndex ] );
    }
  
  }

  /* ionViewDidLoad... https://ionicframework.com/docs/v2/api/navigation/NavController/ */
  
  ionViewWillEnter() {
    if ( this.itemService.selected.id !== null) {
      
      let objIndex : number = this.itemService.fullList.findIndex(( (obj : any ) => obj.id == this.itemService.selected.id ));
      this.loadSubstituteSelectedList( this.itemService.fullList[objIndex].substitute );
    } else {
      this.substituteSelectedList = [];
    }
  }

  public add( item : any ) {

    let itemSubstitute : any = {};
    itemSubstitute.item_id = this.itemService.selected.id;
    itemSubstitute.substitute_item_id = item.id;

    this.itemSubstituteService.post( itemSubstitute )
                               .subscribe ( ( data : any ) => { let objIndex : number = this.substituteToSelectFullList.findIndex(( (obj : any ) => obj.id == data.substitute_item_id ));
                                                                this.global.addToList( this.substituteSelectedList, this.substituteToSelectFullList[objIndex] );
                                                                this.global.deleteFromList( this.substituteToSelectList, this.substituteToSelectFullList[objIndex] ); 
                                                              }
                                          , ( err  : any ) => { this.global.handleError("ERROR_SAVING", err) } );

  }

  public delete( item : any ) {

    let itemSubstitute : any = {};
    let objIndex : number = this.itemSubstituteService.fullList.findIndex(( (obj : any ) => ( obj.item_id == this.itemService.selected.id && obj.substitute_item_id == item.id ) )); 
    itemSubstitute = this.itemSubstituteService.fullList[ objIndex ];

    this.itemSubstituteService.delete( itemSubstitute )
                               .subscribe ( ( data : any ) => { this.global.addToList( this.substituteToSelectList, item );
                                                                this.global.deleteFromList( this.substituteSelectedList, item );
                                                              }
                                          , ( err  : any ) => { this.global.handleError("ERROR_SAVING", err); } );
  }

  public filter( ev : any ) {

    this.substituteToSelectList = this.substituteToSelectFullList;
    var val = ev.target.value;
    if (val && val.trim() != '') {
      this.substituteToSelectList = this.substituteToSelectList.filter(( obj : any ) => {
        return (obj.description.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }

  }


}