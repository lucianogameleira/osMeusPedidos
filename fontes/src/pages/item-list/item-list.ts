import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { GlobalProvider }  from '../../providers/global-provider';
import { ItemProvider }  from '../../providers/item-provider';

@Component({
  selector: 'page-item-list',
  templateUrl: 'item-list.html'
})

export class ItemListPage {

  public alimentGroup : any;
  public search : string = '';
  
  constructor( public navCtrl       : NavController
             , public navParams     : NavParams
             , public itemService   : ItemProvider
             , public global        : GlobalProvider ) {


    if ( !this.itemService.getItensFromLocal() ) { 
      this.itemService.getByCompany(); 
    }
    this.alimentGroup = 'A';

  }

  public doRefresh( refresher : any ) {

    this.itemService.getByCompany();
    refresher.complete();

  }

/* private ionViewDidLoad() {  } */

  public filter () {
    this.itemService.list = this.itemService.fullList.slice(0);
    this.itemService.list = this.itemService.list.filter( ( item : any ) => this.checkValid( item ) );
  }

  private checkValid( item : any ) {

    let tfText : boolean = ( item.description.toLowerCase().indexOf(this.search.toLowerCase()) > -1 );
    
    if ( this.alimentGroup == 'L' && tfText ) { 
      return !this.global.stringToBool(item.tf_solid);
      //return !this.itemService.typeList[objIndex].solid;
    } else if ( this.alimentGroup == 'S' && tfText ) {
      return this.global.stringToBool(item.tf_solid);
      //return this.itemService.typeList[objIndex].solid;
    } else if ( this.alimentGroup == 'A' && tfText ){
      return true;
    } else {
      return false;
    }

  }

  public select ( item : any ) {

    this.itemService.setSelected( item.id );
    this.navCtrl.parent.select( 1 ); // ItemMainPage

  }

  public add() {

    this.itemService.clearSelected();
    this.navCtrl.parent.select(1); // ItemMainPage

  }

}