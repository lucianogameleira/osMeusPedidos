import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { GlobalProvider }  from '../../providers/global-provider';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  
  startDate = '2017-01-01';
  positionX = 0;
  positionY = 0;

  constructor( public navCtrl: NavController
             , public navParams: NavParams
             , public global: GlobalProvider ) {}

}