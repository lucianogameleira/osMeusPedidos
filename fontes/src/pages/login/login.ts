import { Component } from '@angular/core';
// import { NgForm } from '@angular/forms';

import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { SignupPage } from '../signup/signup';
import { ClientTabPage } from '../client-tab/client-tab';

import { UserData } from '../../providers/user-data';
import { TranslateService } from 'ng2-translate';

import {Validators, FormBuilder, FormControl } from '@angular/forms';
import { GlobalProvider }  from '../../providers/global-provider';
import { UserProvider }  from '../../providers/user-provider';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-user',
  templateUrl: 'login.html'
})

export class LoginPage {

  loginForm : any = {};

  login: {username?: string, password?: string} = {};
  submitted = false;

  constructor( public navCtrl: NavController
             , public storage: Storage
             , public userData: UserData
             , public translate: TranslateService
             , public userService : UserProvider
             , public global: GlobalProvider
             , public formBuilder : FormBuilder
             ) {
      this.loginForm = this.formBuilder.group({
      email : ['', Validators.compose( [ Validators.minLength(9)
                                       , Validators.required
                                       , this.isEmail
                                       ] 
                                     )
        ],
      password : ['', Validators.compose( [ Validators.minLength(6)
                                          , Validators.required 
                                          ] ) 
                 ]
    });
  }

  isEmail(control: FormControl): any {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(control.value);
      if (re){
        return null;
      }
      return {"invalidEmail": true};
    }

  onSignup() {
    this.navCtrl.push(SignupPage);
  }

  onChangeLanguage(lang : string) {
    this.translate.use(lang);
    this.global.languageInUse = lang;
    this.storage.set('lastLanguage', this.global.languageInUse);
  }

  onLogin( data: any ) {
      // console.log('data:');console.log (data[0]);
      if ( data.length > 0 ) {
        this.userData.login(data[0].username);
        this.navCtrl.push(ClientTabPage);
      } else {
        this.global.handleError("ERROR_USER_NOTE_FOUND");
      }
  }

  postLoginForm (){
   // console.log('this.loginForm.value:');console.log (this.loginForm.value);
   this.loginForm.value.sistema_origem_id = this.global.AppID;
   this.loginForm.value.username = this.loginForm.value.email;
   this.userService.getByEmail( this.loginForm.value.email )
                   .subscribe ( ( data : any ) => { this.onLogin(data); }
                              , ( err  : any ) => { this.global.handleError("ERROR_GETTING", err); } 
                              );
  }

}