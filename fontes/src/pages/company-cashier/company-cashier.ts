import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { GlobalProvider }  from '../../providers/global-provider';
import { CompanyUserProvider }  from '../../providers/company-user-provider';

@Component({
  selector: 'page-company-cashier',
  templateUrl: 'company-cashier.html'
})

export class CompanyCashierPage {

  public searchCashier: string = '';
  public searchCompanyUser : string = '';
  public cashierList : any = [];
  
  constructor( public navCtrl       : NavController
             , public navParams     : NavParams
             , public companyUserService   : CompanyUserProvider
             , public global        : GlobalProvider ) {

    if ( !this.companyUserService.getItensFromLocal() ) { 
      // this.companyUserService.getByCompanyUser(); 
    }

  }

  public validaCahier (){
    console.log( this.searchCashier );
  }

  public doRefresh( refresher : any ) {

    this.companyUserService.getByCompanyUser();
    refresher.complete();

  }

  ionViewWillEnter() { 

    this.cashierList = this.companyUserService.fullList.filter( ( data : any ) => ( data.perfil.toUpperCase() == 'S' ) );
    console.log('ionViewDidLoad CompanyCashierPage'); 
    
  }


  public filter () {
    this.companyUserService.list = this.companyUserService.fullList.slice(0);
    this.companyUserService.list =this.companyUserService.list.filter( ( data : any ) => this.checkValid( data ) );
  }

  private checkValid( data : any ) {

    let tfText : boolean = ( data.description.toLowerCase().indexOf(this.searchCompanyUser.toLowerCase()) > -1 );
    return tfText;
  }

  public select ( companyUser : any ) {

    this.companyUserService.setSelected( companyUser.id );
    this.navCtrl.parent.select( 1 ); // CompanyUserMainPage

  }

  public add() {

    this.companyUserService.clearSelected();
    this.navCtrl.parent.select(1); // CompanyUserMainPage

  }

}


// WEBPACK FOOTER //
// ./src/pages/company-cashier/company-cashier.ts


// WEBPACK FOOTER //
// ./src/pages/company-cashier/company-cashier.ts


// WEBPACK FOOTER //
// ./src/pages/company-cashier/company-cashier.ts


// WEBPACK FOOTER //
// ./src/pages/company-cashier/company-cashier.ts


// WEBPACK FOOTER //
// ./src/pages/company-cashier/company-cashier.ts


// WEBPACK FOOTER //
// ./src/pages/company-cashier/company-cashier.ts


// WEBPACK FOOTER //
// ./src/pages/company-cashier/company-cashier.ts