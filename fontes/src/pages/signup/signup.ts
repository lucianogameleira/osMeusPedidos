import { Component } from '@angular/core';
import {Validators, FormBuilder, FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { ClientTabPage } from '../client-tab/client-tab';

import { GlobalProvider } from '../../providers/global-provider';
import { UserData } from '../../providers/user-data';
import { UserProvider } from '../../providers/user-provider';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-user',
  templateUrl: 'signup.html'
})

export class SignupPage {

  signupForm : any = {};
  users :any [];
  id : number = 0;
  userServiceTemp : UserProvider;

  signup: {username?: string, password?: string} = {};
  submitted = false;

  constructor( public navCtrl: NavController
             , public formBuilder : FormBuilder
             , public userData: UserData
             , public userService : UserProvider
             , public global: GlobalProvider) {

    this.signupForm = this.formBuilder.group({
      email : ['', Validators.compose( [ Validators.minLength(9)
                                       , Validators.required
                                       , this.isEmail
                                       ] 
                                     )
                 , this.checkEmail.bind(this)
        ],
      password_hash : ['', Validators.compose( [ Validators.minLength(6)
                                          , Validators.required 
                                          ] ) 
                 ]
    });

  }

  isEmail(control: FormControl): any {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(control.value);
      if (re){
        return null;
      }
      return {"invalidEmail": true};
    }

  checkUsername(control: FormControl): any {
    return new Promise( resolve => { this.userService.getUserByLogin( control.value )
                                                    .subscribe( ( data : any ) => { if ( typeof data[0] !== "undefined") {
                                                                                      if ( typeof data[0].username === "undefined") {
                                                                                          resolve(null);
                                                                                      } else { resolve( { "INVALID_USER_NAME": true } ); }
                                                                                    } else { resolve(null);}
                                                                                  }
                                                              , ( err : any ) => { this.global.handleError( "ERROR_GETTING", err ); }
                                                              )
                                });
  }

  checkEmail(control: FormControl): any {

    return new Promise( resolve => { this.userService.getByEmailHTTP( control.value )
                                                     .subscribe( ( data : any ) => { if ( typeof data[0] !== "undefined") {
                                                                                       if ( typeof data[0].email === "undefined") {
                                                                                         resolve(null);
                                                                                       } else { resolve( { "INVALID_EMAIL": true } ); }
                                                                                     } else { resolve(null); }
                                                                                    }
                                                              ,  ( err : any ) => { this.global.handleError( "ERROR_GETTING", err ); }
                                                              )
    });
  }

  postSignupForm (){

   this.signupForm.value.origin_system_id = this.global.AppID;

   this.signupForm.value.username = this.signupForm.value.email;
   
   this.userService.post( this.signupForm.value )
                   .subscribe( ( data : any ) => { this.onSignup(data); }
                             , ( err  : any ) => { this.global.handleError( "ERROR_GETTING", err ); }
    )

    
  }

  onSignup(data: any) {
    this.userData.signup(data.username);
    // TODO : VerificarPerfil e Logar
    this.navCtrl.push(ClientTabPage);
  }

  onFacebook(form: NgForm) {
    console.log( form.value );   
  }


}