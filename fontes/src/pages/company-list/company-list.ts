import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { GlobalProvider }  from '../../providers/global-provider';
import { CompanyProvider }  from '../../providers/company-provider';

@Component({
  selector: 'page-company-list',
  templateUrl: 'company-list.html'
})

export class CompanyListPage {

  public searchCompany : string = '';
  
  constructor( public navCtrl       : NavController
             , public navParams     : NavParams
             , public companyService   : CompanyProvider
             , public global        : GlobalProvider ) {


    if ( !this.companyService.getItensFromLocal() ) { 
      this.companyService.getByCompany(); 
    }

  }

  public doRefresh( refresher : any ) {

    this.companyService.getByCompany();
    refresher.complete();

  }

/* private ionViewDidLoad() {  } */

  public filter () {
    this.companyService.list = this.companyService.fullList.slice(0);
    this.companyService.list =this.companyService.list.filter( ( data : any ) => this.checkValid( data ) );
  }

  private checkValid( data : any ) {

    let tfText : boolean = ( data.description.toLowerCase().indexOf(this.searchCompany.toLowerCase()) > -1 );
    return tfText;
  }

  public select ( company : any ) {

    this.companyService.setSelected( company.id );
    this.navCtrl.parent.select( 1 ); // CompanyMainPage

  }

  public add() {

    this.companyService.clearSelected();
    this.navCtrl.parent.select(1); // CompanyMainPage

  }

}


// WEBPACK FOOTER //
// ./src/pages/company-list/company-list.ts