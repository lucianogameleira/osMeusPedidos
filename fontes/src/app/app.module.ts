import { NgModule } from '@angular/core';

import { IonicApp, IonicModule } from 'ionic-angular';

import { IonicStorageModule } from '@ionic/storage';

import { IsisApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { SchedulePage } from '../pages/schedule/schedule';
import { ScheduleFilterPage } from '../pages/schedule-filter/schedule-filter';
import { SessionDetailPage } from '../pages/session-detail/session-detail';
import { SignupPage } from '../pages/signup/signup';
import { SpeakerDetailPage } from '../pages/speaker-detail/speaker-detail';
import { SpeakerListPage } from '../pages/speaker-list/speaker-list';
import { TabsPage } from '../pages/tabs/tabs';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SupportPage } from '../pages/support/support';

import { GlobalProvider }  from '../providers/global-provider';
import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import { UserProvider }  from '../providers/user-provider';


import { ItemProvider }  from '../providers/item-provider';
import { ItemCompositionProvider }  from '../providers/item-composition-provider';
import { ItemSubstituteProvider }  from '../providers/item-substitute-provider';
import { ItemTabPage } from  '../pages/item-tab/item-tab';
import { ItemListPage } from  '../pages/item-list/item-list';
import { ItemMainPage } from  '../pages/item-main/item-main';
import { ItemCompositionPage } from  '../pages/item-composition/item-composition';
import { ItemSubstitutePage } from  '../pages/item-substitute/item-substitute';

import { ClientTabPage } from  '../pages/client-tab/client-tab';
import { ClientMainPage } from  '../pages/client-main/client-main';

import { CompanyProvider }  from '../providers/company-provider';
import { CompanyTabPage } from '../pages/company-tab/company-tab';
import { CompanyListPage } from '../pages/company-list/company-list';
import { CompanyMainPage } from '../pages/company-main/company-main';
import { CompanyMorePage } from '../pages/company-more/company-more';
import { CompanyCashierPage } from '../pages/company-cashier/company-cashier';
import { CompanyCookPage } from '../pages/company-cook/company-cook';
import { CompanyAreaPage } from '../pages/company-area/company-area';
import { CompanyBartenderPage } from '../pages/company-bartender/company-bartender';
import { CompanyBedroomPage } from '../pages/company-bedroom/company-bedroom';
import { CompanyTablePage } from '../pages/company-table/company-table';

import { CompanyUserProvider }  from '../providers/company-user-provider';

import { HttpModule, Http } from '@angular/http';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';
import { TextMaskModule } from 'angular2-text-mask';

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
  declarations: [
    IsisApp,
    AboutPage,
    AccountPage,
    LoginPage,
    MapPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SignupPage,
    SpeakerDetailPage,
    SpeakerListPage,
    TabsPage,
    TutorialPage,
    SupportPage,
    ItemTabPage,
	ItemMainPage,
	ItemCompositionPage,
	ItemSubstitutePage,
  	ItemListPage,
  	ClientTabPage,
  	ClientMainPage,
  	CompanyTabPage,
  	CompanyListPage,
  	CompanyMainPage,
  	CompanyMorePage,
  	CompanyCashierPage,
	CompanyCookPage,
	CompanyAreaPage,
	CompanyBartenderPage,
	CompanyBedroomPage,
	CompanyTablePage
  ],
  imports: [
	IonicModule.forRoot(IsisApp),
	HttpModule,
	TranslateModule.forRoot({
		provide: TranslateLoader,
		useFactory: (createTranslateLoader),
		deps: [Http]
	}),
	IonicStorageModule.forRoot(),
    TextMaskModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    IsisApp,
    AccountPage,
    AboutPage,
    LoginPage,
    MapPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SignupPage,
    SpeakerDetailPage,
    SpeakerListPage,
    TabsPage,
    TutorialPage,
    SupportPage,
    ItemTabPage,
	ItemMainPage,
	ItemCompositionPage,
	ItemSubstitutePage,
  	ItemListPage,
  	ClientTabPage,
  	ClientMainPage,
  	CompanyTabPage,
  	CompanyListPage,
  	CompanyMainPage,
  	CompanyMorePage,
  	CompanyCashierPage,
	CompanyCookPage,
	CompanyAreaPage,
	CompanyBartenderPage,
	CompanyBedroomPage,
	CompanyTablePage
  ],
  providers: [  GlobalProvider,
  				ConferenceData, 
  				UserData, 
  				Storage, 
  				UserProvider,
  				ItemProvider,
  				ItemCompositionProvider,
  				ItemSubstituteProvider,
  				CompanyProvider,
  				CompanyUserProvider
  			]
})

export class AppModule { }


// WEBPACK FOOTER //
// ./src/app/app.module.ts