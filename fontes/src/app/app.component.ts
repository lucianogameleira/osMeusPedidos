import { Component, ViewChild } from '@angular/core';

import { Events, MenuController, Nav, Platform } from 'ionic-angular';
import { Splashscreen } from 'ionic-native';
import { Storage } from '@ionic/storage';

import { AboutPage } from '../pages/about/about';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
// import { TabsPage } from '../pages/tabs/tabs';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SupportPage } from '../pages/support/support';
import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';

import { GlobalProvider }  from '../providers/global-provider';

import { ItemTabPage } from '../pages/item-tab/item-tab';
import { ItemListPage } from '../pages/item-list/item-list';

import { ClientTabPage } from  '../pages/client-tab/client-tab';
import { ClientMainPage } from  '../pages/client-main/client-main';

import { CompanyTabPage } from '../pages/company-tab/company-tab';
import { CompanyListPage } from '../pages/company-list/company-list';

import { TranslateService } from 'ng2-translate';

export interface PageInterface {
  title: string;
  component: any;
  icon: string;
  logsOut?: boolean;
  index?: number;
  tabComponent?: any;
}

@Component({
  templateUrl: 'app.template.html'
})
export class IsisApp {

  langs = ['en', 'pt-br', 'pt'];

  // the root nav is a child of the root app component
  // @ViewChild(Nav) gets a reference to the app's root nav
  @ViewChild(Nav) nav: Nav;

  // List of pages that can be navigated to from the left menu
  // the left menu only works after login
  // the login page disables the left menu

/*
  cadastrosPages: PageInterface[] = [
    { title: 'SCHEDULE', component: TabsPage, tabComponent: SchedulePage, icon: 'calendar' },
    { title: 'SPEAKERS', component: TabsPage, tabComponent: SpeakerListPage, index: 1, icon: 'contacts' },
    { title: 'MAP', component: TabsPage, tabComponent: MapPage, index: 2, icon: 'map' },
  ];
*/

  allPages: PageInterface[] = [];
  
  appPages: PageInterface[] = [
    { title: 'MNU_PRINCIPAL', component: ClientTabPage, tabComponent: ClientMainPage, icon: 'apps' }
  ];

  registrationPages: PageInterface[] = [
    { title: 'MNU_ITEM', component: ItemTabPage, tabComponent: ItemListPage, icon: 'basket' },
    { title: 'COMPANY', component: CompanyTabPage, tabComponent: CompanyListPage, icon: 'globe' }
  ];

  loggedInPages: PageInterface[] = [
    { title: 'ABOUT', component: AboutPage, index: 3, icon: 'information-circle' },
    { title: 'ACCOUNT', component: AccountPage, icon: 'person' },
    { title: 'SUPPORT', component: SupportPage, icon: 'help' },
    { title: 'LOGOUT', component: LoginPage, icon: 'log-out', logsOut: true }
  ];

  loggedOutPages: PageInterface[] = [
    { title: 'ABOUT', component: AboutPage, index: 3, icon: 'information-circle' },
    { title: 'LOGIN', component: LoginPage, icon: 'log-in' },
    { title: 'SUPPORT', component: SupportPage, icon: 'help' },
    { title: 'SIGNUP', component: SignupPage, icon: 'person-add' }
  ];

  rootPage: any;
  lastLanguage : string = null;

  constructor(
    public events: Events,
    public userData: UserData,
    public menu: MenuController,
    public platform: Platform,
    public confData: ConferenceData,
    public storage: Storage,
    public translate: TranslateService,
    public global : GlobalProvider
  ) {

    this.storage.get('lastLanguage')
                .then( (valor : any ) => {
                                           if ( valor != null ) { 
                                               this.lastLanguage = valor; 
                                           }
                                         });

    this.storage.get('hasSeenTutorial')
      .then((hasSeenTutorial) => {
        if (hasSeenTutorial) {
        this.userData.hasLoggedIn()
                     .then( (hasLoggedIn) => { this.enableMenu(hasLoggedIn === true);
                                               if ( hasLoggedIn ) {
                                                 this.rootPage = ClientTabPage;
                                               } else {
                                                 this.rootPage = LoginPage;
                                               }
                                             }
                          );

          // this.rootPage = LoginPage;
        } else {
          this.rootPage = TutorialPage;
        }
        this.platformReady(translate)
      });


    this.listenToLoginEvents();
  }

  openPage(page: PageInterface) {
    // the nav component was found using @ViewChild(Nav)
    // reset the nav to remove previous pages and only have this page
    // we wouldn't want the back button to show in this scenario
    if (page.index) {
      this.nav.setRoot(page.component, { tabIndex: page.index });
    } else {
      this.nav.setRoot(page.component).catch(() => {
        console.log("Didn't set nav root");
      });
    }

    if (page.logsOut === true) {
      this.userData.logout();
      this.nav.setRoot(LoginPage);
    }
  }

  openTutorial() {
    this.nav.setRoot(TutorialPage);
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:signup', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:logout', () => {
      this.enableMenu(false);
    });
  }

  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn, 'loggedInMenu');
    this.menu.enable(!loggedIn, 'loggedOutMenu');
  }

  platformReady(translate: TranslateService) {
    // Call any initial plugins when ready
    this.platform.ready().then(() => {
      Splashscreen.hide();

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // StatusBar.styleDefault();
      Splashscreen.hide();

      translate.addLangs(this.langs);
      translate.setDefaultLang('en');
      if ( this.lastLanguage == null ) {
        this.global.languageLocal = translate.getBrowserLang();
        if (this.global.languageLocal === 'en' || this.global.languageLocal === 'pt-br' || this.global.languageLocal === 'pt') {
          translate.use( this.global.languageLocal );
        }
        this.global.languageInUse = translate.currentLang;
        this.storage.set('lastLanguage', this.global.languageInUse);
      } else {
        translate.use( this.lastLanguage );
        this.global.languageInUse = this.lastLanguage;
      }
    });
  }

  onChangeLanguage(lang: string) {
    this.translate.use(lang);
    this.global.languageInUse = lang;
    this.storage.set('lastLanguage', this.global.languageInUse);
    this.menu.toggle();
  }

  isActive(page: PageInterface) {
    let childNav = this.nav.getActiveChildNav();

    // Tabs are a special case because they have their own navigation
    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }

    if (this.nav.getActive() && this.nav.getActive().component === page.component) {
      return 'primary';
    }
    return;
  }
}