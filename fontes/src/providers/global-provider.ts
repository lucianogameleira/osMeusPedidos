import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { TranslateService } from 'ng2-translate';
import { ActionSheetController, ToastController, Platform, LoadingController } from 'ionic-angular';
import { Camera, Transfer } from 'ionic-native';
import { Storage } from '@ionic/storage';

declare var cordova: any;

@Injectable()
export class GlobalProvider {

  private formHeader      : string = 'application/json';
  private loadingPopup    : any;

  public AppID            : number = 1;
  public CompanyID        : number = 1;
  public serverAdress     : string = 'http://isissoftwares.esy.es'; // localhost - 192.168.0.16 - isissoftwares.esy.es //
  public wsServerAdress   : string = this.serverAdress + '/isisWS/public';
  public imgServerAdress  : string = this.serverAdress + '/image/';
  public serverApiKey     : string = 'keywordapi_key:87fd0842be8454da3063361d336189da097ae5830cded00c2c42a6fc39e9f188';
  public timeOut          : number = 20000;
  public timeOutHTTP      : number = 20000;
  public timeOutMSGSucesso  : number = 2000;
  public timeOutMSGErro     : number = 5000;
  public timeOutMSGPerentual: number = 1000;
  public languageLocal      : string = ''; 
  public languageInUse      : string = ''; 

  public percentage : number = 100;
  
  headers : Headers = new Headers();

  constructor( public http: Http
             , public toastCtrl: ToastController
             , public translate: TranslateService
             , public actionSheetCtrl: ActionSheetController
             , public platform: Platform
             , public loadingCtrl: LoadingController
             , public storage: Storage ) {

    this.headers.append('Authorization', this.serverApiKey );
    this.headers.append('Content-Type', this.formHeader );
    
  }

  public msgError ( text : string ) {
    this.translate.get( text ).subscribe( value => {
      let toast = this.toastCtrl.create( { message: value
                                         , duration: this.timeOutMSGErro
                                         , position: 'top'
                                         , showCloseButton: true
                                       });
      toast.present();
       }
    );    
  }

  public handleError ( text : string, err : any = null ) {
    console.log( 'User Error:', text )
    console.log( 'Tecnical Error:', err )
    this.translate.get( text ).subscribe( value => {
      let toast = this.toastCtrl.create( { message: value
                                         , duration: this.timeOutMSGErro
                                         , position: 'bottom'
                                         , showCloseButton: true
                                       });
      toast.present();
       }
    );
    try { this.loadingPopup.dismiss(); } catch(err) { console.log( 'loadingPopupError', err.message ) }    
  }

  public msg ( text : string ) {
    this.translate.get( text ).subscribe( value => {
      let toast = this.toastCtrl.create( { message: value
                                         , duration: this.timeOutMSGSucesso
                                         , position: 'top'
                                         , showCloseButton: true
                                       });
      toast.present();
       }
    );
  }

  public msgPercentual ( text : string ) {
    this.translate.get( text )
                  .subscribe( value => {
                                          let toast = this.toastCtrl.create( { message: value
                                                                             , duration: this.timeOutMSGPerentual
                                                                             , position: 'middle'
                                                                             , showCloseButton: true
                                                                           });
                                          toast.present();
                                       }
                            );
  }


  public startProgress () {
    this.translate.get('IN_PROGRESS').subscribe( value => {
                                                        this.loadingPopup = this.loadingCtrl.create({
                                                          content: value
                                                        });
                                                        this.loadingPopup.present();
                                                     }
    );
  }

  public endProgress () {
    this.loadingPopup.dismiss(); 
  }
    
  public getPicture(sourceType : any) {
   // allowEdit: true,
   // saveToPhotoAlbum: true,
    var options = {
      quality: 50,
      sourceType: sourceType,
      correctOrientation: true,
      destinationType: 1,
      targetHeight: 600,
      targetWidth: 600
    };
    return Camera.getPicture(options);
  }

  public createFileName() {
    var d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";
    return newFileName;
  }

  public uploadImage( pImagePath : any, pFileName : any, pDestinationFolder : string) {
    var options = {
      fileKey: "file",
      fileName: pFileName,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : { 'fileName': pFileName
               , 'folder' : pDestinationFolder
               }
    };
    const fileTransfer = new Transfer();

    fileTransfer.onProgress((progressEvent: any ) => {
            if (progressEvent.lengthComputable) {
                let progress = Math.round((progressEvent.loaded / progressEvent.total) * 100);
                this.percentage = progress;
            }
        });

    return fileTransfer.upload(pImagePath, this.imgServerAdress + 'upload.php', options, true);

  }

  public deleteFromList ( list : any, item : any ) {
    let i : number = 0;
    for( i = 0; i < list.length; i++ ) {
      if (list[i] == item){
        list.splice(i, 1);
      }
    }
  }

  public addToList ( list : any, item : any ) {
    list.push( item ); 
  }

  public updateToList ( list : any, item : any ) {
    var objIndex : number = list.findIndex(( (obj : any ) => obj.id == item.id ));
    list[objIndex] = item;
  }

  public stringToBool( val : string ) {
    return (val + '').toLowerCase() === 'true';
  }
    
  public changeLanguage(lang: string) {
    this.translate.use(lang);
    this.languageInUse = lang;
    this.storage.set('lastLanguage', lang);
  }

}


// WEBPACK FOOTER //
// ./src/providers/global-provider.ts


// WEBPACK FOOTER //
// ./src/providers/global-provider.ts