import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { GlobalProvider }  from '../providers/global-provider';
 
@Injectable()
export class UserProvider {

  public selected : any;

  constructor(public http: Http, public global: GlobalProvider) {
    
  }
    
    postUser( params : any ) {
        return this.http.post(this.global.wsServerAdress + '/usuario', params, { headers: this.global.headers, method : "POST" } ).map(res=>res.json())
    }

    getUserByLogin( login : string ) {
        return this.http.get(this.global.wsServerAdress + '/usuario/pornomeusuario/'+ login, { headers: this.global.headers } ).map(res=>res.json())
    }

    public getByEmail( userEmail : string ) {
      this.global.startProgress();
      return this.getByEmailHTTP( userEmail )
                 .map( ( data : any ) => { this.global.endProgress(); 
                                           return data;
                                         } );
    }

  private onSave( data : any ) {

    this.selected = data;
    this.global.endProgress();
    this.global.msg("SAVED_WITH_SUCCESS"); 

  }

  public post( item : any ) {

    this.global.startProgress();

    return this.postHTTP( item )
        .map( ( data : any ) => { this.onSave(data[0]); 
                                  return data[0];
                                }
            , ( err : any ) => {  this.global.endProgress(); 
                                  this.global.handleError("ERROR_SAVING", err);
                                  return err; 
                                }
            )

  } 
  
  public postHTTP( params : any ) {
      return this.http.post( this.global.wsServerAdress + '/user'
                           , params
                           , { headers: this.global.headers, method : "POST" } )
                           .timeout(this.global.timeOutHTTP)
                           .map( res=>res.json() );
  }

    public getByEmailHTTP( userEmail : string ) {
      return this.http.get( this.global.wsServerAdress + '/user/byemail/'+ userEmail
                          , { headers: this.global.headers,  } )
                      .timeout(this.global.timeOutHTTP)
                      .map( res=>res.json() );
    }
      
}