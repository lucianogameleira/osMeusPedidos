import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { GlobalProvider }  from '../providers/global-provider';
import { ItemProvider }  from '../providers/item-provider';

@Injectable()
export class ItemCompositionProvider {

  public selected : any = {};
  public list     : any = [];
  public fullList : any = [];

  constructor( public http: Http
             , public global: GlobalProvider
             , public itemService : ItemProvider
             ) {
  }

  public getItensFromItemSelected () {
    this.fullList = this.itemService.fullList[this.itemService.selectedIndex].composition.slice(0);
  }

  public getByItem( itemId : any ) {
    this.global.startProgress();
    return this.getByItemHTTP( itemId )
               .map( ( data : any ) => { this.global.endProgress(); 
                                               this.fullList = data;
                                               this.list= this.fullList.slice(0); 
                                               return data;
                                             }
                         , ( err  : any ) => { this.global.endProgress(); 
                                               this.global.handleError("ERROR_GETTING", err); 
                                               return err;
                                             }
                         )
  }

  public getByItemHTTP( itemId : any ) {
    return this.http.get( this.global.wsServerAdress + '/item-composition/byitem/'+ itemId
                        , { headers: this.global.headers,  } )
                    .timeout(this.global.timeOutHTTP)
                    .map( res=>res.json() );
  }

  private onSave( data : any ) {

    this.selected = data;
    this.global.endProgress();
    this.global.msg("SAVED_WITH_SUCCESS"); 

  }

  public post( item : any ) {

    this.global.startProgress();

    return this.postHTTP( item )
        .map( ( data : any ) => { this.onSave(data[0]); 
                                  this.global.addToList ( this.fullList, data[0] );
                                  this.global.addToList ( this.list, data[0] );
                                  this.global.addToList( this.itemService.fullList[this.itemService.selectedIndex].composition, data[0] );
                                  return data[0];
                                }
                  , ( err : any ) => { this.global.endProgress(); 
                                       this.global.handleError("ERROR_SAVING", err);
                                       return err; 
                                     }
                  )

  } 
  
  public postHTTP( params : any ) {
      return this.http.post( this.global.wsServerAdress + '/item-composition'
                           , params
                           , { headers: this.global.headers, method : "POST" } )
                           .timeout(this.global.timeOutHTTP)
                           .map( res=>res.json() );
  }

  public delete( item : any ) {

    this.global.startProgress();

    return this.deleteHTTP(item)
               .map( ( data : any ) =>{ this.global.deleteFromList ( this.fullList, item );
                                        this.global.endProgress();
                                        this.global.deleteFromList( this.itemService.fullList[this.itemService.selectedIndex].composition, item );
                                        return data;
                                      }
                   , ( err : any ) => { this.global.endProgress(); 
                                        this.global.handleError("ERROR_DELETING", err); 
                                        return err;
                                      }
                    );

  }

  public deleteHTTP( params : any ) {
    return this.http.delete( this.global.wsServerAdress + '/item-composition/'+ params.id
                           , { headers: this.global.headers, method : "DELETE" } )
                           .timeout(this.global.timeOutHTTP)
                           .map( res=>res.json() );
  } 
  
}


// WEBPACK FOOTER //
// ./src/providers/item-composition-provider.ts


// WEBPACK FOOTER //
// ./src/providers/item-composition-provider.ts