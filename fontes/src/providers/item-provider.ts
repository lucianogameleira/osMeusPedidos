import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { GlobalProvider }  from '../providers/global-provider';

@Injectable()
export class ItemProvider {

  public selected : any = {};
  public selectedIndex : number = null;
  public list     : any = [];
  public fullList : any = [];
  public localItemListID : string = 'item.fullListByCompany';

  public typeList = [ { value : "G", description : "GARNISH"    , solid : true  }
                    , { value : "D", description : "DRINK"      , solid : false }
                    , { value : "S", description : "SNECKS"     , solid : true  }
                    , { value : "F", description : "FOOD"       , solid : true  }
                    , { value : "E", description : "DESSERT"    , solid : true  }  
                    , { value : "A" ,description : "APPETIZER"  , solid : true  }
                    ];

  public liquidUnitList = [ { value : "L"    , description : "LITER"}
                         , { value : "ML"   , description : "MILILITER" }
                         ];

  public solidUnitList = [ { value : "UND"  , description : "UNITY"}
                         , { value : "G"    , description : "GRAM"}
                         , { value : "KG"   , description : "KILOGRAM"}
                         ];


  constructor( public http: Http
             , public global: GlobalProvider 
             , public storage: Storage
             ) {
    this.clearSelected();
  }

  setSelected ( itemID : number ) {
    let objIndexFullList : number = this.fullList.findIndex(( (obj : any ) => obj.id == itemID ));
    if ( objIndexFullList > -1 ) {
      this.selectedIndex = objIndexFullList;
      this.selected = this.fullList[ this.selectedIndex ];
    } else {
      this.global.msgError('ERROR_NOT_FOUND');
    }
  }

  clearSelected () { 
    this.selected = {};
    this.selected.id  = null;
    this.selected.company_id  = this.global.CompanyID;
    this.selected.tf_need_preparation  = 'true';
    this.selected.tf_allows_cooking_steak = 'false';
    this.selected.preparation_time_min  = '1';
    this.selected.img_url = 'assets/img/general/photo-camera.svg'; 
  }

  public getItensFromLocal () {
    this.storage.get(this.localItemListID)
            .then( ( data : any ) => {
                                       if ( data == null ) {
                                           return false;
                                       } else{
                                           this.fullList = data;
                                           this.list= this.fullList.slice(0); 
                                           return true;
                                       }
                                     }
                 , ( err : any ) => { return false; } 
                 );
  }

  public getByCompany() {
    this.global.startProgress();
    return this.getByCompanyHTTP()
               .subscribe( ( data : any ) => { this.global.endProgress(); 
                                               this.fullList = data;
                                               this.storage.set( this.localItemListID, this.fullList );
                                               this.list= this.fullList.slice(0); 
                                             }
                         , ( err  : any ) => { this.global.endProgress(); 
                                               this.global.handleError("ERROR_GETTING", err); 
                                             }
                         )
  }

  public getByCompanyHTTP() {
    return this.http.get( this.global.wsServerAdress + '/full-item/bycompany/'+ this.global.CompanyID
                        , { headers: this.global.headers,  } )
                    .timeout(this.global.timeOutHTTP)
                    .map( res=>res.json() );
  }

  private onSave( data : any ) {

    this.selected = data;
    this.global.endProgress();
    this.global.msg("SAVED_WITH_SUCCESS"); 

  }

  public save( item : any ) {

    this.global.startProgress();

    if ( item.id === null ) {
      return this.postHTTP( item )
          .map( ( data : any ) => { 
                                    let itemLocal : any = data[0];
                                    itemLocal.composition = [];
                                    itemLocal.substitute = [];
                                    this.onSave(itemLocal); 
                                    this.global.addToList ( this.fullList, itemLocal );
                                    this.global.addToList ( this.list, itemLocal );
                                    this.storage.set( this.localItemListID, this.fullList );
                                    return data[0];
                                  }
                    , ( err : any ) => { this.global.endProgress(); 
                                         this.global.handleError("ERROR_SAVING", err);
                                         return err; 
                                       }
                    )
    } else {
      return this.putHTTP( item )
          .map( ( data : any ) => { this.onSave( data[0] ); 
                                    this.global.updateToList ( this.fullList, data[0] )
                                    this.global.updateToList ( this.list, data[0] )
                                    this.storage.set( this.localItemListID, this.fullList );
                                    return data[0];
                                  }
                , ( err : any ) => { this.global.endProgress(); 
                                     this.global.handleError("ERROR_UPDATING", err); 
                                     return err;
                                   }
                )
            }

  }

  public postHTTP( params : any ) {
      return this.http.post( this.global.wsServerAdress + '/item'
                           , params
                           , { headers: this.global.headers, method : "POST" } )
                           .timeout(this.global.timeOutHTTP)
                           .map( res=>res.json() );
  }
    
  public putHTTP( params : any ) {
    return this.http.put( this.global.wsServerAdress + '/item/'+ params.id
                        , params, { headers: this.global.headers, method : "PUT" } 
                        ).timeout(this.global.timeOutHTTP)
                        .map( res=>res.json() );
  }

  public delete( item : any ) {

    this.global.startProgress();

    return this.deleteHTTP(item)
               .subscribe( ( data : any ) =>{ this.global.deleteFromList ( this.fullList, item );
                                              this.global.deleteFromList ( this.list, item );
                                              this.storage.set( this.localItemListID , this.fullList );
                                              this.clearSelected ();
                                              this.global.endProgress();
                                            }
                         , ( err : any ) => { this.global.endProgress(); 
                                              this.global.handleError("ERROR_DELETING", err); 
                                            }
      );

  }

  public deleteHTTP( params : any ) {
      return this.http.delete( this.global.wsServerAdress + '/item/'+ params.id
                             , { headers: this.global.headers, method : "DELETE" } )
                             .timeout(this.global.timeOutHTTP)
                             .map( res=>res.json() );
  }   
    
  public getAll() {
    return this.http.get( this.global.wsServerAdress + '/item'
                        , { headers: this.global.headers }  )
                        .timeout(this.global.timeOutHTTP)
                        .map( res=>res.json() );
  }

  public get( id : number ) {
      return this.http.get( this.global.wsServerAdress + '/item/'+ id
                          , { headers: this.global.headers } )
                          .timeout(this.global.timeOutHTTP)
                          .map( res=>res.json() );
  }

  public getByDescripition( login : string ) {
      return this.http.get( this.global.wsServerAdress + '/item/bydescription/'+ login
                          , { headers: this.global.headers } )
                          .timeout(this.global.timeOutHTTP)
                          .map( res=>res.json() );
  }

}


// WEBPACK FOOTER //
// ./src/providers/item-provider.ts