import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { GlobalProvider }  from '../providers/global-provider';


@Injectable()
export class CompanyProvider {

  public selected : any = {};
  public selectedIndex : number = null;
  public list     : any = [];
  public fullList : any = [];
  public localCompanyListID : string = 'company.fullListByCompany';

  public typeList = [ { value : "H", description : "HOTEL" }
                    , { value : "R", description : "RESTAURANT" }
                    , { value : "T", description : "TECNOLOGY" }
                    , { value : "P", description : "PUB" }
                    ];

  constructor( public http: Http
             , public global: GlobalProvider 
             , public storage: Storage
             ) {
    this.clearSelected();
  }

  setSelected ( companyID : number ) {
    let objIndexFullList : number = this.fullList.findIndex(( (obj : any ) => obj.id == companyID ));
    if ( objIndexFullList > -1 ) {
      this.selectedIndex = objIndexFullList;
      this.selected = this.fullList[ this.selectedIndex ];
    } else {
      this.global.msgError('ERROR_NOT_FOUND');
    }
  }

  clearSelected () { 
    this.selected = {};
    this.selected.id  = null;
    this.selected.company_id  = this.global.CompanyID;
    this.selected.tf_need_preparation  = 'true';
    this.selected.tf_allows_cooking_steak = 'false';
    this.selected.preparation_time_min  = '1';
    this.selected.img_url = 'assets/img/general/photo-camera.svg'; 
  }

  public getItensFromLocal () {
    this.storage.get(this.localCompanyListID)
            .then( ( data : any ) => {
                                       if ( data == null ) {
                                           return false;
                                       } else{
                                           this.fullList = data;
                                           this.list= this.fullList.slice(0); 
                                           return true;
                                       }
                                     }
                 , ( err : any ) => { return false; } 
                 );
  }

  public getByCompany() {
    this.global.startProgress();
    return this.getByCompanyHTTP()
               .subscribe( ( data : any ) => { this.global.endProgress(); 
                                               this.fullList = data;
                                               this.storage.set( this.localCompanyListID, this.fullList );
                                               this.list= this.fullList.slice(0); 
                                             }
                         , ( err  : any ) => { this.global.endProgress(); 
                                               this.global.handleError("ERROR_GETTING", err); 
                                             }
                         )
  }

  public getByCompanyHTTP() {
    return this.http.get( this.global.wsServerAdress + '/full-company'
                        , { headers: this.global.headers,  } )
                    .timeout(this.global.timeOutHTTP)
                    .map( res=>res.json() );
  }

  private onSave( data : any ) {

    this.selected = data;
    this.global.endProgress();
    this.global.msg("SAVED_WITH_SUCCESS"); 

  }

  public save( company : any ) {

    this.global.startProgress();

    if ( company.id === null ) {
      return this.postHTTP( company )
          .map( ( data : any ) => { 
                                    let companyLocal : any = data[0];
                                    companyLocal.composition = [];
                                    companyLocal.substitute = [];
                                    this.onSave(companyLocal); 
                                    this.global.addToList ( this.fullList, companyLocal );
                                    this.global.addToList ( this.list, companyLocal );
                                    this.storage.set( this.localCompanyListID, this.fullList );
                                    return data[0];
                                  }
                    , ( err : any ) => { this.global.endProgress(); 
                                         this.global.handleError("ERROR_SAVING", err);
                                         return err; 
                                       }
                    )
    } else {
      return this.putHTTP( company )
          .map( ( data : any ) => { this.onSave( data[0] ); 
                                    this.global.updateToList ( this.fullList, data[0] )
                                    this.global.updateToList ( this.list, data[0] )
                                    this.storage.set( this.localCompanyListID, this.fullList );
                                    return data[0];
                                  }
                , ( err : any ) => { this.global.endProgress(); 
                                     this.global.handleError("ERROR_UPDATING", err); 
                                     return err;
                                   }
                )
            }

  }

  public postHTTP( params : any ) {
      return this.http.post( this.global.wsServerAdress + '/company'
                           , params
                           , { headers: this.global.headers, method : "POST" } )
                           .timeout(this.global.timeOutHTTP)
                           .map( res=>res.json() );
  }
    
  public putHTTP( params : any ) {
    return this.http.put( this.global.wsServerAdress + '/company/'+ params.id
                        , params, { headers: this.global.headers, method : "PUT" } 
                        ).timeout(this.global.timeOutHTTP)
                        .map( res=>res.json() );
  }

  public delete( company : any ) {

    this.global.startProgress();

    return this.deleteHTTP(company)
               .subscribe( ( data : any ) =>{ this.global.deleteFromList ( this.fullList, company );
                                              this.global.deleteFromList ( this.list, company );
                                              this.storage.set( this.localCompanyListID , this.fullList );
                                              this.clearSelected ();
                                              this.global.endProgress();
                                            }
                         , ( err : any ) => { this.global.endProgress(); 
                                              this.global.handleError("ERROR_DELETING", err); 
                                            }
      );

  }

  public deleteHTTP( params : any ) {
      return this.http.delete( this.global.wsServerAdress + '/company/'+ params.id
                             , { headers: this.global.headers, method : "DELETE" } )
                             .timeout(this.global.timeOutHTTP)
                             .map( res=>res.json() );
  }   
    
  public getAll() {
    return this.http.get( this.global.wsServerAdress + '/company'
                        , { headers: this.global.headers }  )
                        .timeout(this.global.timeOutHTTP)
                        .map( res=>res.json() );
  }

  public get( id : number ) {
      return this.http.get( this.global.wsServerAdress + '/company/'+ id
                          , { headers: this.global.headers } )
                          .timeout(this.global.timeOutHTTP)
                          .map( res=>res.json() );
  }

  public getByDescripition( login : string ) {
      return this.http.get( this.global.wsServerAdress + '/company/bydescription/'+ login
                          , { headers: this.global.headers } )
                          .timeout(this.global.timeOutHTTP)
                          .map( res=>res.json() );
  }

}


// WEBPACK FOOTER //
// ./src/providers/company-provider.ts